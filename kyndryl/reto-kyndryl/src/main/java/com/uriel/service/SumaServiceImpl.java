package com.uriel.service;


import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.uriel.dao.SumaDao;
import com.uriel.entity.Suma;
@Component
public class SumaServiceImpl implements SumaService{
	@Resource 
	SumaDao sumaDao;
        
	@Override
	public int insertSuma(Suma suma) {
		return sumaDao.insertSuma(suma);
	}
}
